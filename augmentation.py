from datasets import load_dataset
import os
import openai
from dotenv import load_dotenv
import json
import ast

load_dotenv()

# env stuff
OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')
HF_WRITE_TOKEN = os.getenv('HF_WRITE_TOKEN')
openai.api_key = OPENAI_API_KEY

# base prompt that query, items and properties will be put into
base_prompt = '''
Below is a SparQL query on Wikidata:
{query}

{items}

{properties}
Return only the following JSON-formatted dictionary:
{{
"prompt": a prompt that would prompt an LLM to write this query
}}
'''

# system prompt for gpt
system_prompt = "You are a code translator who translates SparQL queries into JSON dictionaries."

# custom mapping function for dataset.map()
def augment_data(q):
    # format items and properties for inclusion in the prompt
    items = ""
    if len(q['items']) > 0:
        items += "Query items:\n"
        for i in q['items']:
            items += f"{i[0]} = {i[1]}\n"

    properties = ""
    if len(q['properties']) > 0:
        properties += "Query properties:\n"
        for i in q['properties']:
            items += f"{i[0]} = {i[1]}\n"

    format_break = False
    while not format_break:
        # actually call gpt-3.5-turbo
        response = openai.ChatCompletion.create(
          model="gpt-3.5-turbo",
          messages=[
                {"role": "system", "content": system_prompt},
                {"role": "user", "content": base_prompt.format(query=q['query'], items=items, properties=properties)}
            ]
        )

        # check if first char is "{" and last char is "}" to avoid JSON errors
        if response['choices'][0]['message']['content'][0] == "{" and response['choices'][0]['message']['content'][-1] == "}":
            format_break = True

    try:
        # content = json.loads(response['choices'][0]['message']['content'].replace("'", '"')) # not great choice because some prompts may contain an apostrophe
        content = ast.literal_eval(response['choices'][0]['message']['content'])
        q['prompt'] = content['prompt']
    except:
        q['prompt'] = ""
    return q

if __name__ == "__main__":
    ds = load_dataset('htriedman/wiki-sparql', split='train')
    ds = ds.filter(lambda q: len(q['items']) + len(q['properties']) <= 12)
    ds = ds.train_test_split(train_size=70_000, test_size=10_000, shuffle=True, seed=42)
    ds = ds.map(augment_data, num_proc=16)
    ds.push_to_hub('htriedman/wiki-sparql', token=HF_WRITE_TOKEN)
