from collections import defaultdict
import re
from datasets import load_dataset
import requests
import pickle
from dotenv import load_dotenv

load_dotenv()

HF_WRITE_TOKEN = os.getenv('HF_WRITE_TOKEN')

items = defaultdict(str)
props = defaultdict(str)
base_url = 'https://www.wikidata.org/w/api.php?action=wbgetentities&props=labels&languages=en&languagefallback=1&format=json&ids='
errs = set()

def add_context(q):
    q_items = []
    q_props = []
    
    # regex to get all properties and values
    ids = [i[0] for i in re.findall(r"((P|Q)\d+)", q['query'])]

    # see if properties and values already have been looked up; if yes, remove them from the list
    for i in ids:
        if i in items:
            q_items.append((i, items[i]))
            ids.remove(i)

        elif i in props:
            q_props.append((i, props[i]))
            ids.remove(i)
        
    for i in ids:
        resp = requests.get(f'{base_url}{i}').json()
        try:
            if resp['success'] == 1:
                for k, v in resp['entities'].items():
                    try:
                        if v['type'] == 'item':
                            items[k] = v['labels']['en']['value']
                            q_items.append((k, v['labels']['en']['value']))
                        elif v['type'] == 'property':
                            props[k] = v['labels']['en']['value']
                            q_props.append((k, v['labels']['en']['value']))
                    except:
                        errs.add(i)
            else:
                errs.add(i)
        except:
            errs.add(i)

    q['items']= q_items
    q['properties'] = q_props
    return q
    
if __name__ == '__main__':
    ds = load_dataset('htriedman/wiki-sparql', split='train')
    ds = ds.filter(lambda x: 'SELECT' in x['query'])
    ds = ds.map(add_context)
    ds.push_to_hub('htriedman/wiki-sparql', token=HF_WRITE_TOKEN)
    with open('/home/htriedman/sparql_api_errs.pkl', 'wb') as f:
        pickle.dump(errs, f)